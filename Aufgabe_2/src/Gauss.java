import java.util.Arrays;

public class Gauss {

    /**
     * Diese Methode soll die Loesung x des LGS R*x=b durch
     * Rueckwaertssubstitution ermitteln.
     * PARAMETER:
     * R: Eine obere Dreiecksmatrix der Groesse n x n
     * b: Ein Vektor der Laenge n
     */
    public static double[] backSubst(double[][] R, double[] b) {
        //TODO: Diese Methode ist zu implementieren
    	
    	double[] lv = new double[b.length];
    	
    	for(int i = R.length-1;i >= 0;i--) {
    		double nenner = R[i][i];
    		double zähler = b[i];
    		for(int inneri = i+1; inneri < R.length ;inneri++ ) {
    			zähler-=R[i][inneri]*lv[inneri];
    		}
    		lv[i] = zähler/nenner;
    	}
    	
    	
        return lv;
    }

    /**
     * Diese Methode soll die Loesung x des LGS A*x=b durch Gauss-Elimination mit
     * Spaltenpivotisierung ermitteln. A und b sollen dabei nicht veraendert werden.
     * PARAMETER: A:
     * Eine regulaere Matrix der Groesse n x n
     * b: Ein Vektor der Laenge n
     */
    public static double[] solve(double[][] A, double[] b) {
        //TODO: Diese Methode ist zu implementieren
    	double[][] A2;
    	double[] b2 = b.clone();
    	int n = A[0].length;
    	A2 = new double[n][n];
    	for(int index = 0; index < n; index++) {
    		A2[index]=A[index].clone();
    	}
    	
    	for(int j = 0; j < n-1 ; j++) {
    		pivotSwap(j, n, A2, b2);
    		gaußStep(A2, b2, j, n);
    	}
    	
    	
    	
        return backSubst(A2, b2);
    }
    
    private static void gaußStep(double[][] A, double[] b ,int zeile, int n) {
    	int spalte = zeile;
		for(int i = zeile+1; i<n; i++){
			double scalar = -A[i][spalte]/A[zeile][spalte];
			for(int j = spalte; j < n; j++) {
				A[i][j] = scalar * A[zeile][j] + A[i][j];
				
			}
			b[i] = scalar * b[zeile] + b[i];

		}
    }
    
    public static boolean pivotSwap(int j, int n,double[][] A, double[] b) {
    	double max = Double.MIN_VALUE;
    	int maxIndex = j;
    	boolean allnull= true;
    	for(int i = j; i < n; i++) {
     		if(Math.abs(A[i][j])>0.00000000001) {
     			allnull = false;
     		}
    		if(Math.abs(A[i][j]) > Math.abs(max)) {
    			max = Math.abs(A[i][j]);
    			maxIndex = i;
    		}
    	}
    	
    	if(maxIndex!=j) {
    		//XXX Swap result vector as well
    		//A[Zeile = i][spalte = j] // "oberste" Zeile -> i = j
    		double[] temp = A[j].clone();
    		double tempd = b[j];
    		A[j] = A[maxIndex].clone();
    		b[j] = b[maxIndex];
    		A[maxIndex] = temp;
    		b[maxIndex] = tempd;
    		
    	}
    	return allnull;
    }
    
    

    /**
     * Diese Methode soll eine Loesung p!=0 des LGS A*p=0 ermitteln. A ist dabei
     * eine nicht invertierbare Matrix. A soll dabei nicht veraendert werden.
     *
     * Gehen Sie dazu folgendermassen vor (vgl.Aufgabenblatt):
     * -Fuehren Sie zunaechst den Gauss-Algorithmus mit Spaltenpivotisierung
     *  solange durch, bis in einem Schritt alle moeglichen Pivotelemente
     *  numerisch gleich 0 sind (d.h. <1E-10)
     * -Betrachten Sie die bis jetzt entstandene obere Dreiecksmatrix T und
     *  loesen Sie Tx = -v durch Rueckwaertssubstitution
     * -Geben Sie den Vektor (x,1,0,...,0) zurueck
     *
     * Sollte A doch intvertierbar sein, kann immer ein Pivot-Element gefunden werden(>=1E-10).
     * In diesem Fall soll der 0-Vektor zurueckgegeben werden.
     * PARAMETER:
     * A: Eine singulaere Matrix der Groesse n x n
     */
    public static double[] solveSing(double[][] A) {
        if(A.length<=0) {
        	return new double[0];
        }
        
    	double[][] A2;
    	double[] b2 = A[A.length-1].clone();
	    boolean allnull = true;    	
	    int vvector = 0;
    	int n = A[0].length;
    	
    	A2 = new double[n][n];
    	
    	for(int index = 0; index < n; index++) {
    		A2[index]=A[index].clone();
    	}
    	
    	for(int j = 0; j < n ; j++) {
    		if(pivotSwap(j, n, A2, b2)){
    			allnull = false;  
    			vvector = j;
    			break;
    		}
    		gaußStep(A2, b2, j, n);
    	}
    	
    	

    	if(allnull) {return new double[A[0].length];}
    	
    	double[][] T = new double[vvector][vvector];
    	double[] V = new double[vvector];
    	
    	for(int i = 0; i<T.length;i++) {
    		for(int j = 0; j<T.length;j++) {
    			T[i][j] = A2[i][j];
    		}
    	}
    	
    	for(int i = 0; i<T.length;i++) {
    		V[i] = -A2[i][vvector];
    	}
    	
    	double[] p = new double[A.length];
    	double[] x = backSubst(T, V);
    	for(int i = 0; i<x.length;i++) {
    		p[i] = x[i];
    	}
    	
    	p[x.length] = 1;
    	
    	for(int i = x.length+1; i<p.length;i++) {
    		p[i] = 0;
    	}
    	
    	return p;
    	
    	
    			
    }
    


    /**
     * Diese Methode berechnet das Matrix-Vektor-Produkt A*x mit A einer nxm
     * Matrix und x einem Vektor der Laenge m. Sie eignet sich zum Testen der
     * Gauss-Loesung
     */
    public static double[] matrixVectorMult(double[][] A, double[] x) {
        int n = A.length;
        int m = x.length;

        double[] y = new double[n];

        for (int i = 0; i < n; i++) {
            y[i] = 0;
            for (int j = 0; j < m; j++) {
                y[i] += A[i][j] * x[j];
            }
        }

        return y;
    }
}
